<?php get_header(); ?>

<?php 
    $sidebar = is_active_sidebar('sidebar-1');
?>


<div class="row">
   <main class="col-xs-12 col-md-<?php echo $sidebar ? '8' : '12'; ?>" id="main" role="main">
      <?php if (have_posts()) : ?>
         <p class="found-info">Deine Suchergebnisse f&uuml;r <strong><?php echo $s ?></strong></p>
 
         <?php while (have_posts()) : the_post(); ?>
			<article class="post">
				<header>
				    <h1 class="article-h1"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
					<p class="entry-meta">
						Veröffentlicht am <?php the_time('j. F Y'); ?>
					</p>
				</header>
                <div class="entry">
                    <?php the_excerpt(); ?>
                </div>

                <footer class="article-footer">
					<p class="posted-in">Gepostet in <?php the_category(', '); ?></p>
					<?php the_tags( '<p class="post-tags">Tags: ', ', ', '</p>'); ?>
				</footer>
			</article>
         <?php endwhile; ?>
 
         <p class="other-posts"><?php next_posts_link('&laquo; &Auml;ltere Eintr&auml;ge') ?> | <?php previous_posts_link('Neuere Eintr&auml;ge &raquo;') ?></p>
 
      <?php else : ?>
        <div class="nothing-found">
            <h2 class="nothing-found-h2">Leider nix gefunden</h2>
            <p>Sorry, aber nix Passendes dabei.<br>
            Eventuell die Suche noch etwas verfeinern.</p>
            <div class="nothing-found-searchform">
                <?php get_search_form(); ?>
            </div>
        </div>
      <?php endif; ?>
   </main><!-- main -->
 

	<?php if($sidebar) : ?>
		<?php get_sidebar(); ?>
	<?php endif; ?>

</div> 
<?php get_footer(); ?>