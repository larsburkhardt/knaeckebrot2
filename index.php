<?php get_header(); ?>

<?php 
    $sidebar = is_active_sidebar('sidebar-1');
?>


	<div class="row">
        <main class="col-xs-12 col-md-<?php echo $sidebar ? '8' : '12'; ?>" id="main" role="main">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header>
					<h1 class="article-h1"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<p class="entry-meta">
						Veröffentlicht am <?php the_time('j. F Y'); ?>
					</p>
				</header>
				<?php the_post_thumbnail(); ?>
				<section class="entry">
					<?php the_content(); ?>
				</section>
				
				<footer class="article-footer">
					<p class="posted-in">Gepostet in <?php the_category(', '); ?></p>
					<?php the_tags( '<p class="post-tags">Tags: ', ', ', '</p>'); ?>
				</footer>
			</article>
			<?php endwhile; ?>

			<?php knaeckebrot_content_nav( 'nav-below' ); ?>
 
			<?php endif; ?>

			<!-- Content-Widgets -->
            <?php get_template_part('template-parts/sidebars-bottom');?>

			
		</main>

		<?php get_sidebar(); ?>
		
	</div>
		<?php get_footer(); ?>