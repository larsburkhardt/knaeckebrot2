<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h1 class="page-h1"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>

	<?php the_post_thumbnail(); ?>

	<section class="entry">
		<?php the_content(); ?>
	</section>
  </article>

<?php endwhile; endif; ?>
