# Knaeckebrot 2 - Basic Theme for Wordpress

## Instructions
Move the folder "knaeckebrot2" to your theme folder in your Wordpress installation ("wp-content/themes/").
Theme should now appear in the theme section of Wordpress. Activate it.

## Features

* Grid (Flexbox) by [flexboxgrid.com](http://www.flexboxgrid.com)
* Mobile-first
* Responsive Navigation with Toggle Button
* 2 Menu positions (header, footer)
* 2 Content widgets (if you don't need them, delete them in index.php at line 25 and remove the code in functions.php from line 50 to 70)

## Preview
Preview of the theme at [http://knaeckebrot2.larsburkhardt.de](http://knaeckebrot2.larsburkhardt.de)